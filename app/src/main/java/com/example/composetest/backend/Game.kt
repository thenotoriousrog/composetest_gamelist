package com.example.composetest.backend

import java.net.URL

/**
 * Represents a game item in the application
 * @param title: The name of the game
 * @param rating: The current rating of this game
 * @param creator: The name of the creators of the game
 * @param albumArt: A link to the album art for this game
 */
data class Game(
        val title: String,
        val rating: GameRating,
        val creator: String,
        val albumArt: String?
)