package com.example.composetest.backend

/**
 * Describes the ESRB rating of a particular game.
 */
enum class GameRating {
    E,   // Everyone
    E10, // Everyone 10+
    T,   // Teen
    M,   // Mature 17+
    A,   // Adult 18+
    RP   // Rating Pending
}