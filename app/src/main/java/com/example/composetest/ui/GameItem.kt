package com.example.composetest.ui

import android.widget.ImageView
import androidx.compose.foundation.Image
import androidx.compose.foundation.Text
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.imageFromResource
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.loadImageResource
import androidx.compose.ui.res.loadVectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.ui.tooling.preview.Preview
import androidx.ui.tooling.preview.PreviewParameter
import androidx.ui.tooling.preview.PreviewParameterProvider
import com.example.composetest.backend.Game
import com.example.composetest.R

/**
 * A custom provider that helps generate the preview for the GameItem to be displayed!
 */
private class GameItemPreviewProvider(game: Game) : PreviewParameterProvider<Game> {
    override val values: Sequence<Game> = sequenceOf(game)
}

//@Preview
//@Composable
//fun PreviewGameItem(@PreviewParameter(GameItemPreviewProvider::class) args: Game) {
//
//    Column {
//        Spacer(Modifier
//                .fillMaxWidth()
//                .padding(8.dp))
//    }
//
//    // todo: how do I add a click listener to this GameItem??
//
//    // make a new call to generate the GameItem!
//
//}

// TODO: create an activity that has a material theme to apply the list to!

@Preview
@Composable
fun GameItemView() {

    Column(
        Modifier
            .fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {

        Image(
            imageResource(id = R.drawable.ic_baseline_videogame_asset_24),
            Modifier.preferredHeight(100.dp))

        Text(
                text = "Title",
                modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .fillMaxWidth(),
                fontSize = 14.sp,
                textAlign = TextAlign.Center,
        )

        Text(
                text = "Creator Name",
                modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .fillMaxWidth(),
                fontSize = 12.sp,
                textAlign = TextAlign.Center
        )
    }
}