package com.example.composetest.ui.theme

import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val darkBlue = Color(0xFF0081CB)
val blue = Color(0xFF00B0FF)
val lightBlue = Color(0xff69e2ff)
val orange = Color(0xffff6f00)
val darkOrange = Color(0xffc43e00)
val lightOrange = Color(0xffffa040)
val purple = Color(0xff4527a0)
val purpleDark = Color(0xff000070)
val purpleLight = Color(0xff7953d2)
val darkGrey = Color(0xff212121)
val black = Color(0xff000000)
val lightGrey = Color(0xff484848)