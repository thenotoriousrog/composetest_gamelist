package com.example.composetest.ui.theme

import androidx.compose.foundation.contentColor
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
        primary = purple,
        primaryVariant = purpleDark,
        secondary = darkGrey
)

private val LightColorPalette = lightColors(
        primary = blue,
        primaryVariant = darkBlue,
        secondary = orange)

@Composable
fun GameItemTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {

    val colors = if(darkTheme) DarkColorPalette else LightColorPalette

    MaterialTheme(
            colors = colors,
            typography = mainTypography,
            shapes = mainShapes,
            content = content
    )

}